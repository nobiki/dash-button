amazon-dash==0.3.3
certifi==2018.1.18
chardet==3.0.4
idna==2.6
jsonschema==2.6.0
PyYAML==3.12
requests==2.18.4
scapy-python3==0.23
urllib3==1.22
